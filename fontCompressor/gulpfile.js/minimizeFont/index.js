const fs = require('fs');
const path = require('path');
const sass = require('node-sass');
const { generateFonts } = require('fantasticon');
const fontSvgDir = path.resolve(__dirname, 'fonts')
const fontSvgMap = {
  fab: 'fa-brands-400.svg',
  fad: 'fa-duotone-900.svg',
  fal: 'fa-light-300.svg',
  far: 'fa-regular-400.svg',
  fas: 'fa-solid-900.svg',
  fat: 'fa-thin-100.svg',
}

async function ensureDirExist(filePath) {
  const dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirExist(dirname);
  fs.mkdirSync(dirname);
}
async function cleanDir(directory) {
  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });
}

async function getFiles(dir) {
  const dirents = await fs.promises.readdir(dir, { withFileTypes: true });
  const files = await Promise.all(dirents.map((dirent) => {
    const res = path.resolve(dir, dirent.name);
    return dirent.isDirectory() ? getFiles(res) : res;
  }));
  return Array.prototype.concat(...files);
}

module.exports = async (cb) => {
  const projectSrc = path.resolve(__dirname, '../../../templates')
  const projectFiles = await getFiles(projectSrc)
  const publicFontPath = path.resolve(__dirname, '../../../public/html/fonts')
  const finalCSSPath = path.resolve(__dirname, '../../../public/html/css/fontawesome.css')
  const iconNameMap = {
    'fa-triangle-exclamation': 'fa-exclamation-triangle',
    'fa-circle-xmark': 'fa-times-circle',
    'fa-square-check': 'fa-check-square',
    'fa-circle-minus': 'fa-minus-circle',
    'fa-magnifying-glass': 'fa-search'
  }
  const icons = {
    'fab': [],
    'fad': [],
    'fal': [],
    'far': [],
    'fas': [],
    'fat': [],
  }
  
  projectFiles.forEach(async (file) => {
    const fileContent = fs.readFileSync(file).toString()
    const regex = /(fa[a-z]{1})\s(fa-[a-z0-9\-\.]+)/gm;
    let m;
    while ((m = regex.exec(fileContent)) !== null) {
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      if(icons[m[1]].indexOf(m[2]) === -1) {
        icons[m[1]].push(m[2])
      }
    }
  })
  let cssTotal = fs.readFileSync(path.resolve(__dirname, 'fa-core.css')).toString()
  for(let iconPack in icons) {
    if(icons[iconPack].length == 0) continue;
    console.log(iconPack, icons[iconPack])
    const fontSvgPath = fontSvgDir + '/' + fontSvgMap[iconPack]
    const fontFileContent = fs.readFileSync(fontSvgPath).toString()
    const regex = /<glyph\sglyph-name="([a-z0-9\-\.]+)"((?!\/>).)*((\r)?\n((?!\/>).)*)*\/>/gm;
    const svgIconDir = path.resolve(__dirname, `svg/${iconPack}`)
    ensureDirExist(`${svgIconDir}/dwadaw.svg`)
    cleanDir(svgIconDir)
    let m;
    let counter = 0;
    while (m = regex.exec(fontFileContent)) {
      if (m.index === regex.lastIndex) {
          regex.lastIndex++;
      }
      let iconIndex = icons[iconPack].indexOf(`fa-${m[1]}`)
      if(iconNameMap[`fa-${m[1]}`]) {
        iconIndex = icons[iconPack].indexOf(iconNameMap[`fa-${m[1]}`])
      }
      if(iconIndex !== -1) {
        const pathContent = m[0].replace('<glyph', '<path transform="scale(1, -1) translate(0, -512)"') + '\n'
        const fontContent = `<svg width="512" height="512" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg">\n${pathContent}</svg>`
        const svgIconName = icons[iconPack][iconIndex].replace('fa-', '')
        const svgIconPath = `${svgIconDir}/${svgIconName}.svg`
        fs.writeFileSync(svgIconPath, fontContent)
        icons[iconPack].splice(iconIndex, 1);
      }
    }
    const css = await generateFonts({
      inputDir: svgIconDir, // (required)
      outputDir: publicFontPath, // (required)
      name: iconPack,
      fontTypes: ['woff2', 'woff', 'ttf', 'svg'],
      assetTypes: ['css'],
      templates: {
        css: path.resolve(__dirname, 'css.hbs')
      },
      pathOptions: {},
      codepoints: {},
      fontHeight: 512,
      round: undefined, // --
      //descent: -64, // Will use `svgicons2svgfont` defaults
      normalize: undefined, // --
      selector: '.' + iconPack,
      tag: 'i',
      prefix: `fa`,
      fontsUrl: '../fonts',
      formatOptions: {
        svg: {
          centerHorizontally: true,
          centerVertically: true
        }
      },
    }).then(results => results.assetsOut.css);
    cssTotal += css
  }
  fs.writeFileSync(finalCSSPath, cssTotal)
  console.log('remaining icon', icons)
}