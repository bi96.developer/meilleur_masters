<?php

namespace App\Controller\Frontend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Controller\Backend\BaseController;
use FOS\RestBundle\Controller\Annotations as Rest;

class IndexController extends BaseController
{
    /**
     * @Rest\Get("/", name="home")
     */
    public function index(Request $request): Response
    {
        return $this->render('pages/home.html.twig', []);
    }

    /**
     * @Rest\Get("/404", name="404")
     */
    public function notFound(): Response
    {
        return $this->render('pages/404.html.twig');
    }
}

