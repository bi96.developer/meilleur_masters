<?php

namespace App\Controller\Frontend;

use App\Controller\Backend\BaseController;
use FOS\RestBundle\Controller\Annotations as Rest;

class NewsController extends BaseController
{
    /**
     * @Rest\Get("/les-actualites", name="news")
     */
    public function index()
    {
        return $this->render('news/index.html.twig', []);
    }

    /**
     * @Rest\Get("/{slut}-{id}", name="news.detail")
     */
    public function detail($id)
    {
        return $this->render('news/index.html.twig', []);
    }
}