<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;
use App\Annotation\PermissionPublic;
use App\Annotation\Cache;

/**
 * @Rest\Route("/api/school-group")
 */
class SchoolGroupController extends BaseController
{
    /**
     * @Rest\Get("/list")
     * @PermissionPublic
     * @return View
     */
    public function getList(Request $request): View
    {
        return View::create(
            $this->currentService->getList($request),
            Response::HTTP_OK
        );
    }
    
}