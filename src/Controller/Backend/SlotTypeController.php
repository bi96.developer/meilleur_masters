<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;
use App\Annotation\PermissionPublic;
use App\Annotation\Cache;

/**
 * @Rest\Route("/api/slot-type")
 */
class SlotTypeController extends BaseController
{   
    /**
     * @Rest\Post("")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function add(Request $request): View
    {
        return View::create($this->currentService->add($request), Response::HTTP_OK);
    }
    
    /**
     * @Rest\Post("/update")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function update(Request $request): View
    {
        return View::create($this->currentService->update(null, $request), Response::HTTP_OK);
    }
    
    /**
     * @Rest\Delete("/{id}")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function delete(int $id): View
    {
        return View::create(['result' => $this->currentService->delete($id)], Response::HTTP_OK);
    }
    
    /**
     * @Rest\Get("/list")
     * @PermissionPublic
     * @return View
     */
    public function getList(Request $request): View
    {
        return View::create(
            $this->currentRepo->getList($request),
            Response::HTTP_OK
        );
    }
    
}