<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Service\UserService;

class AuthController extends BaseController
{

    /**
     * Authenticate to get token
     * @Rest\Post("/api/auth")
     * @Log
     * @param Request $request
     * @param UserService $userService
     * @return View
     */
    public function authenticate(Request $request, UserService $userService): View
    {
        $user = $userService->authenticate(json_decode($request->getContent(), 1));

        return View::create(
            (array) $user,
            Response::HTTP_OK,
            ["X-W-Password-Encrypted" => $user->password]
        );
    }
}