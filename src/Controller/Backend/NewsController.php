<?php

namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\Log;
use App\Annotation\PermissionAdmin;
use App\Annotation\PermissionPublic;

/**
 * @Rest\Route("/api/news")
 */
class NewsController extends BaseController
{
    /**
     * @Rest\Post("")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function add(Request $request): View
    {
        return View::create($this->currentService->addEntity($request), Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/update")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function update(Request $request): View
    {
        return View::create($this->currentService->updateEntity($request), Response::HTTP_OK);
    }

    /**
     * @Rest\Delete("/{id}")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function delete(int $id): View
    {
        return View::create(['result' => $this->currentService->deleteEntity($id)], Response::HTTP_OK);
    }

    /**
     * @Rest\Post("/upload-photos")
     * @PermissionAdmin
     * @Log
     * @return View
     */
    public function uploadImage(Request $request)
    {
        if (!$request) {
            throw $this->createNotFoundException(
                sprintf('Images does not exist')
            );
        }

        return View::create($this->currentService->uploadPhotosContent($request), Response::HTTP_OK);
    }

    /**
     * @Rest\Get("/list")
     * @PermissionPublic
     * @return View
     */
    public function getList(Request $request): View
    {
        return View::create(
            $this->currentRepo->getList($request),
            Response::HTTP_OK
        );
    }

    /**
     * @Rest\Get("/{id}")
     * @PermissionPublic
     * @return View
     */
    public function getOne($id): View
    {
        return View::create(
            $this->currentService->getById($id),
            Response::HTTP_OK
        );
    }
}