<?php
namespace App\Controller\Backend;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use App\Annotation\PermissionPublic;

/**
 * @Rest\Route("/api/program-speciality")
 */
class ProgramSpecialityController extends BaseController
{
    /**
     * @Rest\Get("/list")
     * @PermissionPublic
     * @return View
     */
    public function getList(Request $request): View
    {
        return View::create(
            $this->currentService->getListBySite($request),
            Response::HTTP_OK
        );
    }
}