<?php

namespace App\Traits;

use Doctrine\ORM\Mapping as ORM;

trait SeoTrait
{

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $seoTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $seoH1;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $seoKeyword;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $seoDescription;

    public function getSeoTitle(): ?string
    {
        return $this->seoTitle;
    }

    public function setSeoTitle(?string $seoTitle): self
    {
        $this->seoTitle = $seoTitle;

        return $this;
    }

    public function getSeoKeyword(): ?string
    {
        return $this->seoKeyword;
    }

    public function setSeoKeyword(?string $seoKeyword): self
    {
        $this->seoKeyword = $seoKeyword;

        return $this;
    }

    public function getSeoDescription(): ?string
    {
        return $this->seoDescription;
    }

    public function setSeoDescription(?string $seoDescription): self
    {
        $this->seoDescription = $seoDescription;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSeoH1(): ?string
    {
        return $this->seoH1;
    }

    public function setSeoH1(string $seoH1): self
    {
        $this->seoH1 = $seoH1;

        return $this;
    }
}
