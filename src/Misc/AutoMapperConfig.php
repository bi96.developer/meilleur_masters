<?php

namespace App\Misc;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;

class AutoMapperConfig implements AutoMapperConfiguratorInterface
{
    
    public function configure(AutoMapperConfigInterface $config): void
    {
        // Default auto mapper config
        //$config->getOptions()->ignoreNullProperties();
        $config->getOptions()->createUnregisteredMappings();
        $config->getOptions()->dontSkipConstructor();
    }
}
