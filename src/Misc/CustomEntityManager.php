<?php

namespace App\Misc;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception\RetryableException;
use Doctrine\ORM\EntityManagerInterface;


class CustomEntityManager
{
    private $em;
    private $mr;

    public function __construct(EntityManagerInterface $em,
                                ManagerRegistry $mr)
    {
        $this->em = $em;
        $this->mr = $mr;
    }

    public function transactional(callable $callback)
    {
        $retries = 0;
        $error = null;
        do {
            $this->beginTransaction();
            
            try {
                $ret = $callback();
                $this->flush();
                $this->commit();
                return $ret;
            } catch (\Exception $e) {
                $this->rollback();
                $this->close();
                $this->resetManager();
                ++$retries;
                $error = $e;
            }
        } while ($retries < 5);
        throw $error;
    }

    public function resetManager()
    {
        $this->em = $this->mr->resetManager();
    }

    public function __call($name, $args) {
        return call_user_func_array([$this->em, $name], $args);
    }
}