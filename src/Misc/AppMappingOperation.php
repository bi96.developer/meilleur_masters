<?php

namespace App\Misc;

use AutoMapperPlus\MappingOperation\DefaultMappingOperation;
use AutoMapperPlus\MappingOperation\MapperAwareOperation;
use AutoMapperPlus\MappingOperation\MapperAwareTrait;

class AppMappingOperation extends DefaultMappingOperation implements MapperAwareOperation
{
    use MapperAwareTrait;

    public function __construct(
        \Psr\Container\ContainerInterface $serviceLocator
    ) {
        $this->serviceLocator = $serviceLocator;
    }

    function setDestinationValue(
        $destination,
        string $propertyName,
        $value
    ): void {
        if ($value instanceof \DateTime) {
            $requestService = $this->serviceLocator->get('App\Service\RequestService');
            $className = $requestService->commonService->getClassName($destination);
            $requestService->logger->info('sourceValue', [$value]);
            $requestService->logger->info('classname', [$className]);
            $requestService->logger->info('value timezone', [$value->getTimezone()->getName()]);

            if (
                strpos($className, 'Output') === (strlen($className) - 6)
                && $value->getTimezone()->getName() !== $requestService->timezone
            ) {
                $requestService->logger->info('timezone', [$requestService->timezone]);

                $value->setTimezone(new \DateTimeZone($requestService->timezone));

                $requestService->logger->info('after change', [$value]);
                $this->getPropertyWriter()->setProperty(
                    $destination,
                    $propertyName,
                    $value->format('c')
                );
                return;
            }
        }
        parent::setDestinationValue($destination, $propertyName, $value);
    }
}