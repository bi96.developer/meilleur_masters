<?php

namespace App\DTO\Media;

class MediaOutput
{
    public $id;
    public $name;
    public $type;
    public $path;
    public $size;
    public $hasWebp;
    public $hasWebm;
    public $hasThumb;
}
