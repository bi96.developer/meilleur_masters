<?php

namespace App\DTO\Media;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddMediaInput
{
    
    /**
     * @Assert\NotBlank(
     *     message = "Name can not be blank"
     * )
     */
    public $name;
    
    /**
     * @Assert\NotBlank(
     *     message = "Type can not be blank"
     * )
     */
    public $type;
    
    /**
     * @Assert\NotBlank(
     *     message = "Path can not be blank"
     * )
     */
    public $path;
    
    /**
     * @Assert\NotBlank(
     *     message = "Size can not be blank"
     * )
     */
    public $size;
}
