<?php

namespace App\DTO\Advertiser;

class AdvertiserOutput
{
    public $id;
    public $email;
    public $firstName;
    public $lastName;
    public $name;
    public $createdDate;
    public $updatedDate;
}
