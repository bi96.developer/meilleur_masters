<?php

namespace App\DTO\Advertiser;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class AdvertiserMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $mapping = $this->autoMapper->getConfiguration()->registerMapping('App\Entity\Advertiser', 'App\DTO\Advertiser\AdvertiserOutput')
            ->forMember('name', function (\App\Entity\Advertiser $advertiser) {
                return $advertiser->getFirstName() . ' ' . $advertiser->getLastName();
            });
    }
}
