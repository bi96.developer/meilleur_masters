<?php

namespace App\DTO\Advertiser;

use Symfony\Component\Validator\Constraints as Assert;

class AddAdvertiserInput
{
    /**
     * @Assert\NotBlank(
     *     message="Email can not be blank"
     * )
     *
     * @Assert\Email(
     *     message="Email invalid"
     * )
     *
     */
    public $email;

    /**
     * @Assert\NotBlank(
     *     message = "First name can not be blank"
     * )
     */
    public $firstName;

    /**
     * @Assert\NotBlank(
     *     message = "Last name can not be blank"
     * )
     */
    public $lastName;

}
