<?php

namespace App\DTO\User;

class UserOutput
{
    public $id;
    public $email;
    public $picture;
    public $cover;
    public $password;
    public $firstName;
    public $lastName;
    public $phone;
    public $authPermitted;
}
