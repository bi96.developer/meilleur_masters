<?php

namespace App\DTO\User;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class UserMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $userListOutput = $config->registerMapping('App\Entity\User', 'App\DTO\User\UserListOutput')
            ->forMember('picture', function (\App\Entity\User $user) {
                return $this->getUserPicture($user);
            })
            ->forMember('subRoles', function (\App\Entity\User $user) {
                if (!$user->getSubRoles()) {
                    return null;
                }
                $roles = [];
                forEach($user->getSubRoles() as $role) {
                    $roles[] = $role->getName();
                }
                return implode(' / ', $roles);
            });
        $config->registerMapping('App\Entity\User', 'App\DTO\User\AdminListOutput')
            ->copyFromMapping($userListOutput)
            ->forMember('token', function (\App\Entity\User $user) {
                return hash('sha256', $user->getPassword());
            });
    }
}
