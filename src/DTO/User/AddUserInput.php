<?php

namespace App\DTO\User;

use Symfony\Component\Validator\Constraints as Assert;
use App\DTO\Validator\ContainsUnique;

class AddUserInput
{
    /**
     * @Assert\NotBlank(
     *     message="Email can not be blank"
     * )
     *
     * @Assert\Email(
     *     message="Email invalid"
     * )
     *
     * @ContainsUnique(
     *     entityClass="App\Entity\User",
     *     field="email",
     *     message="Email has already taken"
     * )
     */
    public $email;

    /**
     * @Assert\NotBlank(
     *     message = "Password can not be blank"
     * )
     * @Assert\Length(
     *     min="6",
     *     minMessage="Password should be longer than 6 characters"
     * )
     */
    public $password;

    /**
     * @Assert\NotBlank(
     *     message = "First name can not be blank"
     * )
     */
    public $firstName;

    /**
     * @Assert\NotBlank(
     *     message = "Last name can not be blank"
     * )
     */
    public $lastName;

    public $phone;
}
