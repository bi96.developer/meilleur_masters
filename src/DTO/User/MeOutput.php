<?php

namespace App\DTO\User;

class MeOutput
{
    public $id;
    public $email;
    public $picture;
    public $password;
    public $firstName;
    public $lastName;
    public $userGroup;
    public $phone;
    public $city;
    public $country;
    public $subRoles;
    public $state;
    
}
