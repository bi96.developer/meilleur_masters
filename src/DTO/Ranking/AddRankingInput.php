<?php

namespace App\DTO\Ranking;

class AddRankingInput
{
    public $programId;
    public $programSpecialityId;
    public $rank;
    public $stars;
    public $year;
}
