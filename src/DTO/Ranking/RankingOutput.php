<?php

namespace App\DTO\Ranking;

class RankingOutput
{
    public $id;
    public $programId;
    public $programSpecialityId;
    public $rank;
    public $stars;
    public $year;
}
