<?php

namespace App\DTO\Advert;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class AdvertMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $mapping = $this->autoMapper->getConfiguration()->registerMapping('App\Entity\Advert', 'App\DTO\Advert\AdvertOutput')
            ->forMember('campaign', function (\App\Entity\Advert $advert) {
                $campaign = $advert->getCampaign();
                return [
                    'id' => $campaign->getId(),
                    'name' => $campaign->getName()
                ];
            })
            ->forMember('slotTypeId', function (\App\Entity\Advert $advert) {
                return $advert->getSlotType()->getId();
            })
            ->forMember('slotTypeName', function (\App\Entity\Advert $advert) {
                return $advert->getSlotType()->getName();
            })
            ;
    }
}
