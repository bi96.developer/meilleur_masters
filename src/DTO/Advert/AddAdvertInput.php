<?php

namespace App\DTO\Advert;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddAdvertInput
{
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\Campaign",
     *     type="entity"
     * )
     */
    public $campaign;
    
    /**
     * @Assert\NotBlank(
     *     message="Link is required"
     * )
     */
    public $link;
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\Media",
     *     type="mediaEntity"
     * )
     */
    public $media;
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\SlotType",
     *     type="entity"
     * )
     */
    public $slotType;
    
    /**
     * @TransformAnnotation(
     *     type="collectionEntity",
     *     class="App\Entity\Page"
     * )
     */
    public $pages;
    
    public $htmlContent;
}
