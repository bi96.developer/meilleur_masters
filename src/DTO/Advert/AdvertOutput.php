<?php

namespace App\DTO\Advert;

class AdvertOutput
{
    public $id;
    public $name;
    public $campaign;
    public $link;
    public $media;
    public $slotType;
    public $slotTypeId;
    public $slotTypeName;
    public $pages;
    public $htmlContent;
    public $createdDate;
    public $updatedDate;
}
