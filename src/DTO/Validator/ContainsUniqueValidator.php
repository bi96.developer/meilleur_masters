<?php

namespace App\DTO\Validator;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ContainsUniqueValidator extends ConstraintValidator
{
    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * @var EntityManagerInterface
     */
    public $entityManager;

    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $entityManager
    ) {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint->entityClass || !class_exists($constraint->entityClass) || !$constraint->field) {
            return true;
        }

        $founded = $this->entityManager->getRepository($constraint->entityClass)->findOneBy([
            $constraint->field => $value
        ]);

        if (!$founded) {
            return true;
        }

        $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value)
            ->addViolation();
    }
}
