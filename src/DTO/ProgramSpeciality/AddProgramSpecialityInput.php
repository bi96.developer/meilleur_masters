<?php

namespace App\DTO\ProgramSpeciality;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddProgramSpecialityInput
{
    /**
     * @Assert\NotBlank(
     *     message="Title is required"
     * )
     */
    public $name;
}