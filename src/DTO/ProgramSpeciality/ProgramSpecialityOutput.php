<?php

namespace App\DTO\ProgramSpeciality;

class ProgramSpecialityOutput
{
    public $id;
    public $name;
}