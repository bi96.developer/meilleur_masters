<?php

namespace App\DTO\NewsProgramSpeciality;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class NewsProgramSpecialityMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {

        $mapping = $this->autoMapper->getConfiguration()->registerMapping('App\Entity\NewsProgramSpeciality', 'App\DTO\NewsProgramSpeciality\NewsProgramSpecialityOutput');
    }
}