<?php

namespace App\DTO\NewsProgramSpeciality;

class NewsProgramSpecialityOutput
{
    public $id;
    public $news;
    public $programSpeciality;
}