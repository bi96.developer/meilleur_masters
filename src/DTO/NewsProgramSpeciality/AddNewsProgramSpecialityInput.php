<?php

namespace App\DTO\NewsProgramSpeciality;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddNewsProgramSpecialityInput
{

    /**
     * @TransformAnnotation(
     *     class="App\Entity\News",
     *     type="entity"
     * )
     */
    public $news;

    /**
     * @TransformAnnotation(
     *     class="App\Entity\ProgramSpeciality",
     *     type="entity"
     * )
     */
    public $programSpeciality;
}