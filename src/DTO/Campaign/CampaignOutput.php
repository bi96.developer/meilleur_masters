<?php

namespace App\DTO\Campaign;

class CampaignOutput
{
    public $id;
    public $name;
    public $advertiser;
    public $status;
    public $startDate;
    public $endDate;
    public $stopAtDate;
    public $stopAtImpression;
    public $stopAtClick;
    public $stopAtConversion;
    public $currentImpression;
    public $currentClick;
    public $currentConversion;
    public $priority;
    public $viewerLimitTotal;
    public $viewerLimitPerSession;
    public $viewerLimitReset;
    public $createdDate;
    public $updatedDate;
    public $adverts;
}
