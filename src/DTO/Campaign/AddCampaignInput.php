<?php

namespace App\DTO\Campaign;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddCampaignInput
{
    
    /**
     * @Assert\NotBlank(
     *     message = "Name can not be blank"
     * )
     */
    public $name;
    
    /**
     * @TransformAnnotation(
     *     class="App\Entity\Advertiser",
     *     type="entity"
     * )
     */
    public $advertiser;
    
    /**
     * @TransformAnnotation(
     *     type="datetime"
     * )
     */
    public $startDate;
    
    /**
     * @TransformAnnotation(
     *     type="datetime"
     * )
     */
    public $stopAtDate;
    
    /**
     * @TransformAnnotation(
     *     type="collectionEntityEditable",
     *     class="App\Entity\Advert"
     * )
     */
    public $adverts;
    
    public $status;
    public $stopAtImpression;
    public $stopAtClick;
    public $stopAtConversion;
    public $priority;
    public $viewerLimitTotal;
    public $viewerLimitPerSession;
    public $viewerLimitReset;
    
}
