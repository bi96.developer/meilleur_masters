<?php

namespace App\DTO\Campaign;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class CampaignMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $mapping = $this->autoMapper->getConfiguration()->registerMapping('App\Entity\Campaign', 'App\DTO\Campaign\CampaignOutput')
            ->forMember('advertiser', function (\App\Entity\Campaign $campaign) {
                $advertiser = $campaign->getAdvertiser();
                return [
                    'id' => $advertiser->getId(),
                    'name' => $advertiser->getName()
                ];
            })
            ->forMember('adverts', function (\App\Entity\Campaign $campaign) {
                return $this->autoMapper->mapMultiple($campaign->getAdverts(), 'App\\DTO\\Advert\AdvertOutput');
            })
            ;
    }
}
