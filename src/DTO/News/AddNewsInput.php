<?php

namespace App\DTO\News;

use Symfony\Component\Validator\Constraints as Assert;
use App\Annotation\TransformAnnotation;

class AddNewsInput
{

    /**
     * @TransformAnnotation(
     *     class="App\Entity\NewsProgramSpeciality",
     *     type="collectionEntityEditable"
     * )
     */
    public $newsProgramSpecialities;
    /**
     * @Assert\NotBlank(
     *     message="Title is required"
     * )
     */
    public $name;


    public $slug;

    public $description;
    public $content;

    public $status = 1;
    public $priority = 1;

    public $seoTitle;
    public $seoKeyword;
    public $seoDescription;
    public $seoH1;
}