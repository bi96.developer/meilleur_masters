<?php

namespace App\DTO\News;

use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use App\Service\BaseService;

class NewsMapper extends BaseService implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {

        $mapping = $this->autoMapper->getConfiguration()->registerMapping('App\Entity\News', 'App\DTO\News\NewsOutput')
            ->forMember('newsProgramSpecialities', function (\App\Entity\News $news) {
                $newsProgramSpecialies = $news->getNewsProgramSpecialities();
                $result = [];
                if (!$newsProgramSpecialies) {
                    return $result;
                }
                foreach ($newsProgramSpecialies as $newsProgramSpeciality) {
                    $programSpecility = $newsProgramSpeciality->getProgramSpeciality();
                    $result[] =  [
                        'id' => $programSpecility->getId(),
                        'name' => $programSpecility->getName()
                    ];
                }
                return $result;
            })
            ->forMember('photo', function (\App\Entity\News $news) {
                return $this->newsService->getPhoto($news);
            });
    }
}