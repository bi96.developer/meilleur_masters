<?php

namespace App\DTO\News;

class NewsOutput
{

    public $newsProgramSpecialities;

    public $id;
    public $name;
    public $description;
    public $content;
    public $photo;

    public $status;
    public $priority;
    public $slug;

    public $seoTitle;
    public $seoKeyword;
    public $seoDescription;
    public $seoH1;

    public $createdDate;
    public $updatedDate;
}