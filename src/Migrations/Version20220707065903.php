<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220707065903 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE slot_page DROP FOREIGN KEY FK_CA231CB759E5119C');
        $this->addSql('ALTER TABLE slot_slot_type DROP FOREIGN KEY FK_3B206A2159E5119C');
        $this->addSql('CREATE TABLE advert_page (advert_id INT NOT NULL, page_id INT NOT NULL, INDEX IDX_5D2AF76DD07ECCB6 (advert_id), INDEX IDX_5D2AF76DC4663E4 (page_id), PRIMARY KEY(advert_id, page_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE advert_page ADD CONSTRAINT FK_5D2AF76DD07ECCB6 FOREIGN KEY (advert_id) REFERENCES advert (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE advert_page ADD CONSTRAINT FK_5D2AF76DC4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE slot');
        $this->addSql('DROP TABLE slot_page');
        $this->addSql('DROP TABLE slot_slot_type');
        $this->addSql('ALTER TABLE advert ADD slot_type_id INT NOT NULL');
        $this->addSql('ALTER TABLE advert ADD CONSTRAINT FK_54F1F40B2B4AEE37 FOREIGN KEY (slot_type_id) REFERENCES slot_type (id)');
        $this->addSql('CREATE INDEX IDX_54F1F40B2B4AEE37 ON advert (slot_type_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE slot (id INT AUTO_INCREMENT NOT NULL, advert_id INT NOT NULL, INDEX IDX_AC0E2067D07ECCB6 (advert_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE slot_page (slot_id INT NOT NULL, page_id INT NOT NULL, INDEX IDX_CA231CB759E5119C (slot_id), INDEX IDX_CA231CB7C4663E4 (page_id), PRIMARY KEY(slot_id, page_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE slot_slot_type (slot_id INT NOT NULL, slot_type_id INT NOT NULL, INDEX IDX_3B206A2159E5119C (slot_id), INDEX IDX_3B206A212B4AEE37 (slot_type_id), PRIMARY KEY(slot_id, slot_type_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E2067D07ECCB6 FOREIGN KEY (advert_id) REFERENCES advert (id)');
        $this->addSql('ALTER TABLE slot_page ADD CONSTRAINT FK_CA231CB759E5119C FOREIGN KEY (slot_id) REFERENCES slot (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE slot_page ADD CONSTRAINT FK_CA231CB7C4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE slot_slot_type ADD CONSTRAINT FK_3B206A212B4AEE37 FOREIGN KEY (slot_type_id) REFERENCES slot_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE slot_slot_type ADD CONSTRAINT FK_3B206A2159E5119C FOREIGN KEY (slot_id) REFERENCES slot (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE advert_page');
        $this->addSql('ALTER TABLE advert DROP FOREIGN KEY FK_54F1F40B2B4AEE37');
        $this->addSql('DROP INDEX IDX_54F1F40B2B4AEE37 ON advert');
        $this->addSql('ALTER TABLE advert DROP slot_type_id');
    }
}
