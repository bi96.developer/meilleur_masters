<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220715082650 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, photo VARCHAR(255) NOT NULL, status INT NOT NULL, priority INT NOT NULL, created_date DATETIME DEFAULT NULL, updated_date DATETIME DEFAULT NULL, slug VARCHAR(255) DEFAULT NULL, seo_title VARCHAR(255) DEFAULT NULL, seo_h1 VARCHAR(255) DEFAULT NULL, seo_keyword LONGTEXT DEFAULT NULL, seo_description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_program_speciality (id INT AUTO_INCREMENT NOT NULL, news_id INT DEFAULT NULL, program_speciality_id INT DEFAULT NULL, INDEX IDX_2FC67F80B5A459A0 (news_id), INDEX IDX_2FC67F809E13184F (program_speciality_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE program_speciality (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE news_program_speciality ADD CONSTRAINT FK_2FC67F80B5A459A0 FOREIGN KEY (news_id) REFERENCES news (id)');
        $this->addSql('ALTER TABLE news_program_speciality ADD CONSTRAINT FK_2FC67F809E13184F FOREIGN KEY (program_speciality_id) REFERENCES program_speciality (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE news_program_speciality DROP FOREIGN KEY FK_2FC67F80B5A459A0');
        $this->addSql('ALTER TABLE news_program_speciality DROP FOREIGN KEY FK_2FC67F809E13184F');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE news_program_speciality');
        $this->addSql('DROP TABLE program_speciality');
    }
}