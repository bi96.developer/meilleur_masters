<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

use App\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220707103515 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAWareTrait;
    
    private $em;
    
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        
    }

    public function down(Schema $schema) : void
    {
        
    }
    
    public function postUp(Schema $schema) : void
    {
        parent::postUp($schema);
        
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        
        $conn = $this->em->getConnection();
        $conn->update(
            'page', 
            [
                'name' => 'Home',
                'type' => 'homepage',
            ],
            [
                'id' => 1
            ]
        );
        $conn->update(
            'page', 
            [
                'name' => 'Spécialité',
                'type' => 'all-specialities',
            ],
            [
                'id' => 2
            ]
        );
        $pages = [
          ['Programme client','program-client'],
          ['Programme non-client','program-non-client'],
          ['Methodologie','methodology'],
          ['École','school'],
          ['Articles','articles'],
          ['Actualités','news'],
          ['Page d\'une actualité','news-detail'],
          ['Orientation','orientation'],
          ['Classement','classement'],
        ];
        forEach($pages as $page) {
            $conn->insert(
                'page', 
                [
                    'name' => $page[0],
                    'type' => $page[1]
                ]
            );
        }
        
        $conn->update(
            'slot_type', 
            [
                'name' => 'Mega banner central',
                'width' => 430,
                'height' => 190,
            ],
            [
                'id' => 1
            ]
        );
        
        $conn->update(
            'slot_type', 
            [
                'name' => 'Expender mega banner up',
                'width' => 400,
                'height' => 280,
            ],
            [
                'id' => 2
            ]
        );
        
        $conn->update(
            'slot_type', 
            [
                'name' => 'Expender low',
                'width' => 400,
                'height' => 280,
            ],
            [
                'id' => 3
            ]
        );
        
        $conn->update(
            'slot_type', 
            [
                'name' => 'Interstitial content',
                'width' => 430,
                'height' => 600,
            ],
            [
                'id' => 4
            ]
        );
        
    }
    
    
}
