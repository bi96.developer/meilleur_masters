<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220707103258 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE slot_type_page (slot_type_id INT NOT NULL, page_id INT NOT NULL, INDEX IDX_AAD2E4B72B4AEE37 (slot_type_id), INDEX IDX_AAD2E4B7C4663E4 (page_id), PRIMARY KEY(slot_type_id, page_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE slot_type_page ADD CONSTRAINT FK_AAD2E4B72B4AEE37 FOREIGN KEY (slot_type_id) REFERENCES slot_type (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE slot_type_page ADD CONSTRAINT FK_AAD2E4B7C4663E4 FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE page ADD type VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE slot_type ADD width INT DEFAULT NULL, ADD height INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE slot_type_page');
        $this->addSql('ALTER TABLE page DROP type');
        $this->addSql('ALTER TABLE slot_type DROP width, DROP height');
    }
}
