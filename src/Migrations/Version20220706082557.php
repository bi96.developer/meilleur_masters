<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220706082557 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE campaign ADD status VARCHAR(255) DEFAULT NULL, ADD end_date DATETIME DEFAULT NULL, ADD current_impression INT DEFAULT NULL, ADD current_click INT DEFAULT NULL, ADD stop_at_conversion INT DEFAULT NULL, ADD current_conversion INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE campaign DROP status, DROP end_date, DROP current_impression, DROP current_click, DROP stop_at_conversion, DROP current_conversion');
    }
}
