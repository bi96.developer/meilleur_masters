<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Symfony\Component\Finder\Finder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

use App\Entity\User;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220704035849 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAWareTrait;
    
    private $em;
    
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        
    }

    public function down(Schema $schema) : void
    {
        
    }
    
    public function postUp(Schema $schema) : void
    {
        parent::postUp($schema);
        
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        
        $this->insertRole();
        
        $this->insertUser();
        
        $this->insertPages();
        
        $this->insertSlotTypes();
        
    }
    
    
    private function insertRole() {
        $query = $this->em->getConnection()->prepare("
            INSERT INTO role (id, name) VALUES
            (1,	'Administrator'),
            (2,	'User');");
        $query->execute();
    }
  
    private function insertUser() {
        $passwordEncoder = $this->container->get("security.password_encoder");
        $username = $this->container->getParameter('admin.username');
        $password = $this->container->getParameter('admin.password');
        
        $conn = $this->em->getConnection();
        $user = new User();
        $user->setFirstName('Super');
        $user->setLastName('Admin');
        $user->setEmail($username);
        $user->setAuthPermitted(true);
        $conn->insert(
            'user', 
            [
                'first_name' => 'Super', 
                'last_name' => 'Admin',
                'email' => $username,
                'password' => $passwordEncoder->encodePassword($user, $password),
                'auth_permitted' => true,
                'created_date' => '2019-09-17 03:36:47',
                'updated_date' => '2019-09-17 03:36:47'
            ]
        );
        $conn->insert(
            'user_role', 
            [
                'role_id' => 1,
                'user_id' => 1,
            ]
        );
        
    }
    
    private function insertPages() {
        $query = $this->em->getConnection()->prepare("
            INSERT INTO page (id, name, spec_id) VALUES
            (1,	'Homepage', null),
            (2,	'All Speciality', null);");
        $query->execute();
    }
    
    private function insertSlotTypes() {
        $query = $this->em->getConnection()->prepare("
            INSERT INTO slot_type (id, name) VALUES
            (1,	'Head banner'),
            (2,	'Expender Up'),
            (3,	'Expender Low'),
            (4,	'Interstitial content');");
        $query->execute();
    }
}
