<?php
namespace App\Service;

use App\Repository\MediaRepository;

/**
 * Class MediaService
 * @package App\Service
 */
class MediaService extends BaseService
{
    /**
     * MediaService constructor.
     * @param MediaRepository $repository
     */
    public function __construct(
        MediaRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
