<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 * @package App\Service
 */
class UserService extends BaseService
{
    /**
     * UserService constructor.
     * @param UserRepository $repository
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(
        UserRepository $repository,
        BaseService $baseService,
        UserPasswordEncoderInterface $encoder
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->encoder = $encoder;
    }

    public function getPermissions($user = null)
    {
        if (empty($user)) {
            $user = $this->getUser();
        }
        $permissions = array();
        forEach ($user->getSubRoles() as $role) {
            if ($role->getName() == 'Administrator') {
                $permissions = $this->permissionRepo->findAll();
                break;
            } else {
                $permissions = array_merge($permissions, $role->getPermissions()->toArray());
            }
        }
        return $permissions;
    }

    public function authenticate(array $authData)
    {
        /** @var User $user */
        $user = $this->repository->findOneBy(['email' => $authData['email']]);
        if(!$user) {
            throw new AccessDeniedException(AccessDeniedException::USER_NOT_FOUND, 'authenticate');
        }
        if(!$user->getAuthPermitted()) {
            throw new AccessDeniedException(AccessDeniedException::NOT_PERMITTED, 'authenticate');
        }
        $correctAuthentity = false;
        if(isset($authData['token'])) {
            $password = hash('sha256', $user->getPassword());
            if(hash_equals($password, $authData['token'])) {
                $correctAuthentity = true;
            }
        } else if ($this->encoder->isPasswordValid($user, $authData['password'])) {
            $correctAuthentity = true;
        }
        if($correctAuthentity) {
            $permissions = $this->getPermissions($user);
            $userDto = $this->autoMapper->map($user, 'App\DTO\User\MeOutput');
            $userDto->password = hash('sha256', $user->getPassword());
            if($this->requestService->isBackend) {
                $userDto->permissions = [];
                forEach ($permissions as $permission) {
                    $userDto->permissions[] = $permission->getAction();
                }
            }
            return $userDto;
        }
        throw new AccessDeniedException(AccessDeniedException::USER_NOT_FOUND, 'authenticate');

    }

    public function register($request)
    {
        $object = $this->inputResolver($request, 'App\DTO\User\AddUserInput');
        $user = $this->objectToEntity($object, 'User', null, true);
        $passwordEncoder = $this->container->get("security.password_encoder");
        $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
        $user->setAuthPermitted(true);
        $this->repository->save($user);
        // $this->sendWelcomeEmail($user, $request->request->get('feUrl'));
        $user = (array) $this->autoMapper->map($user, 'App\DTO\User\MeOutput');
        $user['password'] = hash('sha256', $user['password']);
        return $user;
    }

    public function updatePicture($user, $request)
    {
        if (!$user instanceof User) {
            $user = $this->get($user);
        }

        // Validate photo
        /** @var \App\DTO\User\UpdateUserPhotoInput $input */
        $input = $this->inputResolver($request, 'App\DTO\User\UpdateUserPhotoInput');

        $fileData = explode(';base64,', $input->photoContent);
        if (!isset($fileData[1])) {
            throw new BadRequestHttpException('Invalid input data');
        }

        $fileName = md5(uniqid()) . '.jpg';
        $filePath = '/uploads/user/picture/' . $fileName;
        $this->saveFile(
            $this->getPath($filePath),
            base64_decode($fileData[1])
        );
        $this->mediaService->createThumbnail(
            $this->getPath($filePath),
            $this->getPath($filePath),
            150,
            150
        );

        $user->setPicture($fileName);
        $this->repository->save($user);

        return $this->autoMapper->map($user, 'App\DTO\User\MeOutput');
    }

    /**
     * @param User|int $user
     * @param $request
     * @return string
     */
    public function updateCover($user, $request)
    {
        if (!$user instanceof User) {
            $user = $this->repository->find($user);
        }

        if (!$user) {
            throw new NotFoundHttpException('The user does not exist');
        }

        // Validate photo
        /** @var \App\DTO\User\UpdateUserCoverInput $input */
        $input = $this->inputResolver($request, 'App\DTO\User\UpdateUserCoverInput');

        $fileData = explode(';base64,', $input->coverContent);
        if (!isset($fileData[1])) {
            throw new BadRequestHttpException('Invalid input data');
        }

        $fileName = md5(uniqid()) . '.jpg';
        $filePath = '/uploads/user/cover/' . $fileName;
        $this->saveFile(
            $this->getPath($filePath),
            base64_decode($fileData[1])
        );

        $user->setCover($fileName);
        $this->repository->save($user);

        return $this->autoMapper->map($user, 'App\DTO\User\MeOutput');
    }

    /**
     * @param User|int $user
     * @param $request
     * @return mixed
     * @throws \AutoMapperPlus\Exception\UnregisteredMappingException
     */
    public function updateProfile($user, $request)
    {
        $user = $this->get($user);

        $object = $this->inputResolver($request, 'App\DTO\User\UpdateUserInput');
        /** @var User $user */
        $user = $this->objectToEntity($object, 'User', $user, true);
        $this->repository->save($user);
        $this->cache->delete('User.Detail.' . $user->getId());
        return $this->autoMapper->map($user, 'App\DTO\User\MeOutput');
    }

    /**
     * @param User|int $user
     * @param $request
     */
    public function updateShortBio($user, $shortBio)
    {
        if (!$user instanceof User) {
            $user = $this->get($user);
        }

        $shortBio = $this->commonService->cleanInput($shortBio);

        $user->setShortBio($shortBio);
        $this->repository->save($user);
        $this->cache->delete('User.Detail.' . $user->getId());
        return $this->autoMapper->map($user, 'App\DTO\User\MeOutput');
    }

    public function updatePassword($user, $request)
    {
        if (!$user instanceof User) {
            $user = $this->repository->find($user);
        }

        if (!$user) {
            throw new NotFoundHttpException('The user does not exist');
        }

        /** @var \App\DTO\User\UpdateUserPasswordInput $input */
        $input = $this->inputResolver($request, 'App\DTO\User\UpdateUserPasswordInput');

        $passwordEncoder = $this->container->get("security.password_encoder");

        if (!$passwordEncoder->isPasswordValid($user, $input->currentPassword)) {
            throw new \Exception('The current password is incorrect');
        }
        $newPassword = $passwordEncoder->encodePassword($user, $input->newPassword);
        $user->setPassword($newPassword);

        $this->repository->save($user);

        return hash('sha256', $newPassword);
    }

    public function getList($request, $DTO = 'App\DTO\User\UserListOutput') {
        $extraFilter = null;
        $requestData = $request->query->all();
        if(isset($requestData['filter_name_like'])) {
          $extraFilter = function($queryBuilder) use ($requestData) {
              $queryBuilder->andWhere("CONCAT(User.firstName,' ',User.lastName) LIKE :name");
              $queryBuilder->setParameter('name', '%' . $requestData['filter_name_like'] . '%');
          };
          $request->query->remove('filter_name_like');
        }
        $adminRole = $this->roleRepo->findOneBy(['name' => 'Administrator']);
        $request->query->set('notMemberOf_subRoles', $adminRole->getId());
        $result = $this->repository->getList($request, $DTO, $extraFilter);
        return $result;
    }

    public function getUserDetail($userId) {
        $user = null;
        $cacheKey = 'User.Detail.' . $userId;
        $cachedUser = $this->cache->getItem($cacheKey);

        if($cachedUser->isHit()) {
            $user = json_decode($cachedUser->get(), 1);
        } else {
            $user = $this->get($userId);
            if($this->isAdmin($user)) {
                throw new NotFoundHttpException('User not found');
            }
            $user = (array)$this->autoMapper->map($user, 'App\DTO\User\UserOutput');
            // Cache
            $cachedUser->set(json_encode($user, 1));
            $this->cache->save($cachedUser);
        }
        if(!$user['authPermitted']) {
            return null;
        }
        unset($user['password']);
        return $user;
    }

    public function sendWelcomeEmail($user, $home) {
        $email = $user->getUsername();

        $subjectMail = 'Welcome to Best Broker Community';
        $bodyMail = $this->container->get('twig')->render('email/user/welcome_to_new_user.html.twig', [
            'user' => $user->getFullName(),
            'home' => $home
        ]);

        $message = new \Swift_Message(
            $subjectMail,
            $bodyMail,
            'text/html'
        );

        $message
            ->setFrom(
                $this->container->getParameter('mail.no_reply.from_address'),
                $this->container->getParameter('mail.no_reply.from_name')
            )
            ->setTo($email);

        $this->sendMail($message);
    }

    public function sendContactEmail($data) {
      $message = new \Swift_Message(
        'You have a new contact',
        $this->container->get('twig')->render('email/layouts/contact.html.twig', ['data' => $data]),
        'text/html'
      );
      $recipientsString = $this->container->getParameter('mail.contact.recipients');
      $recipients = explode(",", $recipientsString);
      $message
        ->setFrom(
          $this->container->getParameter('mail.no_reply.from_address'),
          $this->container->getParameter('mail.no_reply.from_name')
        )
        ->setTo($recipients);

      return $this->sendMail($message);
    }

    public function setAuthorization($mode, $userId) {
        $user = $this->get($userId);
        if($this->isAdmin($user)) {
            return ['result' => 'failed'];
        }
        $user->setAuthPermitted(boolval($mode));
        $user->setContentDeleted(false);
        $this->userRepo->save($user);
        $this->cache->delete('User.Detail.' . $user->getId());
        return ['result' => 'success'];
    }

    public function getUserMapped(
        $entity,
        $getHandicap = false,
        $getEmail = false,
        $getUserMethod = null,
        $getLastMessage = false,
        $getRoleString = false
    ) {
        if($getUserMethod) {
          $user = $entity->{$getUserMethod}();
        } else {
          $user = method_exists($entity, 'getProfile') ? $entity->getProfile()->getUser() : $entity->getUser();
        }
        $data = [
            'id' => $user->getId(),
            'name' => $user->getFirstName() . ' ' . $user->getLastName(),
            'picture' => $this->getUserPicture($user)
        ];
        if($getHandicap) {
            $data['handicap'] = $user->getHandicap();
        }
        if($getEmail) {
            $data['email'] = $user->getEmail();
        }
        if($getLastMessage) {
            $data['lastMessage'] = $user->getLastMessage() ? $user->getLastMessage()->format('Y-m-d H:i:s') : null;
            $isOnline = false;
            if($data['lastMessage']) {
                $diff = abs(time() - strtotime($data['lastMessage']));
                if($diff < 240) {
                    $isOnline = true;
                }
            }
            $data['isOnline'] = $isOnline;
        }
        if($getRoleString) {
            $roles = [];
            forEach($user->getSubRoles() as $role) {
                $roles[] = $role->getName();
            }
            $data['subRoles'] = implode(' / ', $roles);
        }
        if(!$user->getAuthPermitted()) {
            $data['banned'] = true;
        }
        return $data;
    }
}
