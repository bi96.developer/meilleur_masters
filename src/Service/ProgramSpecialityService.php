<?php
namespace App\Service;

/**
 * Class ProgramSpecialityService
 * @package App\Service
 */
class ProgramSpecialityService extends BaseService
{
    /**
     * ProgramSpecialityService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
