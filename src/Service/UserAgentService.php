<?php
namespace App\Service;

use App\Repository\UserAgentRepository;

/**
 * Class UserAgentService
 * @package App\Service
 */
class UserAgentService extends BaseService
{
    /**
     * UserAgentService constructor.
     * @param UserAgentRepository $repository
     */
    public function __construct(
        UserAgentRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}
