<?php
namespace App\Service;

/**
 * Class SchoolTypeService
 * @package App\Service
 */
class  SchoolTypeService extends BaseService
{
    /**
     * SchoolTypeService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
