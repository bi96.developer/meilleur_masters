<?php
namespace App\Service;

use App\Repository\RoleRepository;

/**
 * Class RoleService
 * @package App\Service
 */
class RoleService extends BaseService
{
    /**
     * RoleService constructor.
     * @param RoleRepository $repository
     */
    public function __construct(
        RoleRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}
