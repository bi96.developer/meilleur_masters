<?php
namespace App\Service;

use App\Repository\RankingRepository;

/**
 * Class RankingService
 * @package App\Service
 */
class RankingService extends BaseService
{
    /**
     * RankingService constructor.
     * @param RankingRepository $repository
     */
    public function __construct(
        RankingRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
