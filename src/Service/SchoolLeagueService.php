<?php
namespace App\Service;

/**
 * Class SchoolLeagueService
 * @package App\Service
 */
class  SchoolLeagueService extends BaseService
{
    /**
     * SchoolLeagueService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
