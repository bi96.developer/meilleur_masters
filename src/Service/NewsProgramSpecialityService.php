<?php

namespace App\Service;

use App\Repository\NewsProgramSpecialityRepository;

/**
 * Class NewsService
 * @package App\Service
 */
class NewsProgramSpecialityService extends BaseService
{
    /**
     * NewsService constructor.
     * @param NewsProgramSpecialityRepository $repository
     */
    public function __construct(
        NewsProgramSpecialityRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }
}