<?php
namespace App\Service;

use App\Repository\SlotTypeRepository;

/**
 * Class SlotTypeService
 * @package App\Service
 */
class SlotTypeService extends BaseService
{
    /**
     * SlotTypeService constructor.
     * @param SlotTypeRepository $repository
     */
    public function __construct(
        SlotTypeRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
