<?php
namespace App\Service;

/**
 * Class ZoneService
 * @package App\Service
 */
class  ZoneService extends BaseService
{
    /**
     * ZoneService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
