<?php
namespace App\Service;

/**
 * Class ProgramTypeService
 * @package App\Service
 */
class ProgramTypeService extends BaseService
{
    /**
     * ProgramTypeService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
