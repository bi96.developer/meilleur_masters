<?php
namespace App\Service;

use App\Repository\CampaignRepository;

/**
 * Class CampaignService
 * @package App\Service
 */
class CampaignService extends BaseService
{
    /**
     * CampaignService constructor.
     * @param CampaignRepository $repository
     */
    public function __construct(
        CampaignRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
