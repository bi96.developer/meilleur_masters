<?php
namespace App\Service;

use App\Repository\AdvertRepository;

/**
 * Class AdvertService
 * @package App\Service
 */
class AdvertService extends BaseService
{
    /**
     * AdvertService constructor.
     * @param AdvertRepository $repository
     */
    public function __construct(
        AdvertRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
