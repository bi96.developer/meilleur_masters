<?php
namespace App\Service;

/**
 * Class CountryService
 * @package App\Service
 */
class  CountryService extends BaseService
{
    /**
     * CountryService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
