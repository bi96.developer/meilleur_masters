<?php
namespace App\Service;

/**
 * Class ProgramLevelService
 * @package App\Service
 */
class ProgramLevelService extends BaseService
{
    /**
     * ProgramLevelService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
