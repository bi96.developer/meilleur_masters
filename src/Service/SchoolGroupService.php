<?php
namespace App\Service;

/**
 * Class SchoolGroupService
 * @package App\Service
 */
class  SchoolGroupService extends BaseService
{
    /**
     * SchoolGroupService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
