<?php
namespace App\Service;

use App\Repository\AdvertiserRepository;

/**
 * Class AdvertiserService
 * @package App\Service
 */
class AdvertiserService extends BaseService
{
    /**
     * AdvertiserService constructor.
     * @param AdvertiserRepository $repository
     */
    public function __construct(
        AdvertiserRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
