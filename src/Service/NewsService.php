<?php

namespace App\Service;

use App\Constant\Common;
use App\DTO\News\NewsOutput;
use App\Exception\BadRequestException;
use App\Repository\NewsRepository;
use App\Helper\ImageUploadHelper;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class NewsService
 * @package App\Service
 */
class NewsService extends BaseService
{
    /**
     * NewsService constructor.
     * @param NewsRepository $repository
     */
    public function __construct(
        NewsRepository $repository,
        BaseService $baseService,
        ImageUploadHelper $imageUploadHelper
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
        $this->imageUploadHelper = $imageUploadHelper;
    }


    public function getListAllNews($requestData)
    {
        return $this->repository->getListAllNews($requestData, NewsOutput::class);
    }

    public function getListLastedNews()
    {
        return $this->repository->getListLastedNews(NewsOutput::class);
    }

    public function getNewsBySlug($slug)
    {
        return $this->repository->getNewsBySlug($slug, NewsOutput::class);
    }

    public function getById($id)
    {
        return $this->repository->getById($id, NewsOutput::class);
    }


    public function getPhoto($news)
    {
        if (!$news->getPhoto()) {
            return null;
        }
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/news/%s',
                $news->getPhoto()
            )
        );
    }

    public function getPhotos($name)
    {
        if (!$name) {
            return null;
        }
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/news/photos/%s',
                $name
            )
        );
    }

    public function uploadPhoto($fileData, $path)
    {
        $dir = $this->getPath($path);
        if (!file_exists($dir)) {
            mkdir($dir, 0777, true);
            chmod($dir, 0777);
        }

        return $this->imageUploadHelper->save(
            $fileData,
            $this->getPath($path),
        );
    }


    public function uploadPhotosContent($request)
    {
        $result = [];
        $imageName = '';
        $uploaded = 0;
        if ($request->files->get('photo')) {;

            $fileData = $request->files->get('photo');
            $fileName = $this->uploadPhoto($fileData, '/uploads/news/photos');
            if ($fileName) {
                $uploaded = 1;
            }
        }

        $result = [
            'file_name' => $imageName,
            'uploaded' => $uploaded,
            'url' => $this->getPhotos($fileName)
        ];

        return $result;
    }


    public function addEntity($request)
    {

        $newsInput = [];

        // Check Slug Exist
        // if ($request->get('name')) {
        //     $newsInput['slug'] = $this->commonService->slugify($request->get('name'));

        //     $newsResult = $this->repository->findBy(['slug' => $newsInput['slug']]);
        //     if ($newsResult) {
        //         throw new BadRequestException(Common::ERROR_UNIQUE_NAME);
        //     }
        // }

        $entity = $this->add($request, $newsInput, null, 'ENTITY', false, [], true);

        // Upload photo
        $fileData = $request->files->get('photo');
        if ($fileData) {
            $fileName = $this->uploadPhoto($fileData, '/uploads/news');
            $entity->setPhoto($fileName);
        }

        $this->repository->save($entity);

        return (array)$this->autoMapper->map($entity, NewsOutput::class);
    }

    public function updateEntity($request)
    {
        $entity = $this->get($request->get('id'));

        $newsInput = [];

        // Check Slug Exist
        if ($request->get('name')) {
            $newsInput['slug'] = $this->commonService->slugify($request->get('name'));

            $newsResult = $this->repository->checkExistSlug($entity, 'slug', $newsInput['slug']);
            if ($newsResult) {
                throw new BadRequestException(Common::ERROR_UNIQUE_NAME);
            }
        }

        $entity = $this->update($entity, $request, $newsInput, null, 'ENTITY', false, true);

        // Upload photo
        $fileData = $request->files->get('photo');
        if ($fileData) {
            $oldPhoto = $entity->getPhoto();
            $fileName = $this->uploadPhoto($fileData, '/uploads/news');
            $entity->setPhoto($fileName);
        }

        // Remove photo
        if ($request->get('photo_remove') == 'delete') {
            $oldPhoto = $entity->getPhoto();
            $entity->setPhoto('');
        }

        $this->repository->save($entity);

        if (isset($oldPhoto) && $oldPhoto) {
            $this->imageUploadHelper->unlink($oldPhoto, $this->getPath('/uploads/news'));
        }

        return $entity;
    }


    public function deleteEntity($id)
    {
        $entity = $this->get($id);

        $oldPhoto = $entity->getPhoto();
        $this->delete($entity);

        if (isset($oldPhoto) && $oldPhoto) {
            $this->imageUploadHelper->unlink($oldPhoto, $this->getPath('/uploads/news'));
        }
        return true;
    }
}