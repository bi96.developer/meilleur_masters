<?php
namespace App\Service;

/**
 * Class SchoolService
 * @package App\Service
 */
class  SchoolService extends BaseService
{
    /**
     * SchoolService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
