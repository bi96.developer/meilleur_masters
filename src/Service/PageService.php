<?php
namespace App\Service;

use App\Repository\PageRepository;

/**
 * Class PageService
 * @package App\Service
 */
class PageService extends BaseService
{
    /**
     * PageService constructor.
     * @param PageRepository $repository
     */
    public function __construct(
        PageRepository $repository,
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
        $this->repository = $repository;
    }

}
