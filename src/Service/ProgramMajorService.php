<?php
namespace App\Service;

/**
 * Class ProgramMajorService
 * @package App\Service
 */
class ProgramMajorService extends BaseService
{
    /**
     * ProgramMajorService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
