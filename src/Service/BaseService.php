<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Cache\Adapter\RedisAdapter;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use Doctrine\DBAL\LockMode;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use AutoMapperPlus\AutoMapperInterface;
use Psr\Log\LoggerInterface;

use App\Misc\TransformDataHelper;
use App\Misc\AppMappingOperation;
use App\Exception\ValidateException;
use App\Exception\BadRequestException;
use App\Annotation\TransformAnnotation;
use App\Repository\BaseRepository;

class BaseService
{
    public function __construct(
        \Symfony\Component\DependencyInjection\ContainerInterface $container,
        CommonService $commonService,
        AutoMapperInterface $autoMapper,
        TransformDataHelper $transformDataHelper,
        ValidatorInterface $validator,
        TokenStorageInterface $tokenStorage,
        ParameterBagInterface $params,
        BaseRepository $baseRepository,
        UrlHelper $urlHelper,
        CacheInterface $appCache,
        \Psr\Container\ContainerInterface $serviceLocator,
        LoggerInterface $logger,
        AppMappingOperation $appMappingOperation,
        HttpClientInterface $httpClient
    ) {
        $this->container = $container;
        $this->commonService = $commonService;
        $this->autoMapper = $autoMapper;
        $this->autoMapper->getConfiguration()->getOptions()->setDefaultMappingOperation($appMappingOperation);

        $this->transformDataHelper = $transformDataHelper;
        $this->validator = $validator;
        $this->tokenStorage = $tokenStorage;
        $this->params = $params;
        $this->filesystem = new Filesystem();
        $this->entityManager = $baseRepository->getEntityManager();
        $this->urlHelper = $urlHelper;
        $this->cache = $appCache;
        $this->serviceLocator = $serviceLocator;
        $this->logger = $logger;
        $this->httpClient = $httpClient;
    }

    public function reflectFromParent($parent)
    {
        $this->container = $parent->container;
        $this->commonService = $parent->commonService;
        $this->autoMapper = $parent->autoMapper;
        $this->transformDataHelper = $parent->transformDataHelper;
        $this->validator = $parent->validator;
        $this->tokenStorage = $parent->tokenStorage;
        $this->filesystem = $parent->filesystem;
        $this->params = $parent->params;
        $this->entityManager = $parent->entityManager;
        $this->cache = $parent->cache;
        $this->urlHelper = $parent->urlHelper;
        $this->serviceLocator = $parent->serviceLocator;
        $this->logger = $parent->logger;
        $this->httpClient = $parent->httpClient;
    }

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function add(
        $request,
        array $inputs = null,
        String $requestDTO = null,
        String $responseDTO = null,
        Bool $inTransaction = false,
        array $checkExists = [],
        Bool $includeRelations = false,
        callable $beforeMappingCallback = null
    ) {

        $serviceName = $this->commonService->getClassName($this, true);

        if (empty($requestDTO)) {
            $requestDTO = 'App\DTO\\' . $serviceName . '\Add' . $serviceName . 'Input';
        }
        $object = $this->inputResolver($request, $requestDTO, $inputs, $serviceName, $checkExists);
        $this->logger->info('object', [$object]);
        $entity = $this->objectToEntity($object, $serviceName, null, $includeRelations);
        $this->logger->info('entity', [$entity]);
        $this->repository->save($entity, $inTransaction);

        if (empty($responseDTO)) {
            $responseDTO = 'App\DTO\\' . $serviceName . '\\' . $serviceName . 'Output';
        }
        if (strtolower($responseDTO) == 'entity') {
            return $entity;
        }
        if (!is_null($beforeMappingCallback)) {
            call_user_func($beforeMappingCallback, $entity);
        }
        $this->logger->info('start mapping');
        return $this->autoMapper->map($entity, $responseDTO);
    }

    public function update(
        $entity = null,
        $request = null,
        array $inputs = null,
        String $requestDTO = null,
        String $responseDTO = null,
        Bool $inTransaction = false,
        Bool $includeRelations = false,
        callable $beforeMappingCallback = null
    ) {
        $serviceName = $this->commonService->getClassName($this, true);
        if (empty($requestDTO)) {
            $requestDTO = 'App\DTO\\' . $serviceName . '\Update' . $serviceName . 'Input';
            if (!class_exists($requestDTO)) {
                $requestDTO = 'App\DTO\\' . $serviceName . '\Add' . $serviceName . 'Input';
            }
        }
        if (is_null($entity)) {
            $id = array_merge(
                $request->request->all(),
                $request->query->all(),
                json_decode($request->getContent(), 1) ?? []
            )['id'];
            $entity = $this->get($id);
        }
        $object = $this->inputResolver($request, $requestDTO, $inputs, $serviceName);
        $this->objectToEntity($object, $serviceName, $entity, $includeRelations);

        // dd($entity->getNewsProgramSpecialities());

        $this->repository->save($entity, $inTransaction);
        if (empty($responseDTO)) {
            $responseDTO = 'App\DTO\\' . $serviceName . '\\' . $serviceName . 'Output';
        }
        if ($responseDTO == 'ENTITY') {
            return $entity;
        }
        if (!is_null($beforeMappingCallback)) {
            call_user_func($beforeMappingCallback, $entity);
        }
        return $this->autoMapper->map($entity, $responseDTO);
    }

    public function delete(
        $entity = null,
        $filters = [],
        $strict = true
    ) {
        if (is_int($entity)) {
            $entity = $this->get($entity);
        }
        if (is_null($entity)) {
            $entity = $this->repository->findOneBy($filters);
            if (is_null($entity)) {
                if ($strict) {
                    $serviceName = $this->commonService->getClassName($this, true);
                    throw new BadRequestException(
                        BadRequestException::NO_ENTITY_TO_DELETE,
                        null,
                        $serviceName
                    );
                } else {
                    return false;
                }
            }
        }
        $this->repository->delete($entity);
        return true;
    }

    public function inputResolver(
        $request,
        $requestDTO,
        $inputs = null,
        $serviceName = null,
        $checkExists = []
    ) {

        if ($request instanceof Request) {
            $inputArr = array_merge(
                $request->request->all(),
                $request->query->all(),
                json_decode($request->getContent(), 1) ?? []
            );
        } else {
            $inputArr = $request;
        }

        if (!empty($inputs)) {
            $inputArr = array_merge($inputArr, $inputs);
        }
        if (count($checkExists) > 0) {
            $checkExists = array_intersect_key(
                $inputArr,
                array_flip($checkExists)
            );
            $existedEntity = $this->repository->findOneBy($checkExists);
            if (!is_null($existedEntity)) {
                throw new BadRequestException(
                    BadRequestException::DUPLICATE_ENTITY,
                    null,
                    $serviceName
                );
            }
        }
        $inputDTO = $this->autoMapper->map($inputArr, $requestDTO);
        if (isset($inputArr['id'])) {
            $inputDTO->id = $inputArr['id'];
        }
        $properties = array_keys($inputArr);
        $this->logger->info('inputArr', [$inputArr]);
        if (
            $request instanceof Request
            && $request->files->count()
        ) {
            foreach ($request->files->all() as $fieldName => $file) {
                $this->logger->info('fieldName', [$fieldName]);
                $this->logger->info('file', [$file]);
                if (is_array($file)) {
                    if (!is_array($inputDTO->{$fieldName})) {
                        $inputDTO->{$fieldName} = [];
                    }

                    foreach ($file as $index => $childFile) {
                        if (is_array($childFile)) {
                            $inputDTO->{$fieldName}[$index][array_keys($childFile)[0]] = array_values($childFile)[0];
                        } else {
                            $inputDTO->{$fieldName}[$index] = $childFile;
                        }
                    }
                } else {
                    $inputDTO->{$fieldName} = $file;
                    $properties[] = $fieldName;
                }
            }
        }
        $this->logger->info('after inputDTO', [$inputDTO]);
        $this->transformDataHelper->transform($inputDTO, $properties);

        $errors = $this->validator->validate($inputDTO);

        if (count($errors) > 0) {
            $messages = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $messages[$error->getPropertyPath()] = $error->getMessage();
            }
            throw new BadRequestException(
                BadRequestException::WRONG_INPUT,
                null,
                $serviceName,
                null,
                $messages
            );
        }

        return $inputDTO;
    }

    public function objectToEntity(
        $object,
        $entityName,
        $entity = null,
        $includeRelations = false
    ) {
        if (is_null($entity)) {
            $entityClass = 'App\\Entity\\' . $entityName;
            $entity = new $entityClass();
        }
        $metadata = $this->getEntityManager()->getClassMetadata('App\\Entity\\' . $entityName);
        $typeMap = [];
        foreach ($metadata->fieldMappings as $fieldData) {
            $typeMap[$fieldData['fieldName']] = $fieldData['type'];
        }
        if ($includeRelations) {
            foreach ($metadata->associationMappings as $associationMapping) {
                $typeMap[$associationMapping['fieldName']] = $associationMapping['type'];
            }
        }
        $this->logger->info('object data', (array) $object);
        $this->logger->info('typeMap', (array) $typeMap);
        foreach ($object as $key => $value) {
            if (is_null($value)) {
                continue;
            }
            if (
                isset($typeMap[$key])
                && ($typeMap[$key] == ClassMetadataInfo::MANY_TO_MANY
                    || $typeMap[$key] == ClassMetadataInfo::ONE_TO_MANY
                )
            ) {
                $singularizedKey = null;
                $words = preg_split('#([A-Z][^A-Z]*)#', ucwords($key), null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
                $lastUpperCaseWord = $words[count($words) - 1];
                $lastUpperCaseWordSingle = $this->commonService->singularize($lastUpperCaseWord);
                $singularizedKey = str_replace($lastUpperCaseWord, '', ucwords($key)) . $lastUpperCaseWordSingle;
                if (!method_exists($entity, 'add' . $singularizedKey)) {
                    continue;
                }
                $oldRelations = $entity->{'get' . ucwords($key)}();
                $newRelations = $value;
                $currentRelationIdList = [];
                foreach ($oldRelations as $oldRelation) {
                    foreach ($newRelations as $newRelation) {
                        if ($newRelation->getId() == $oldRelation->getId()) {
                            $currentRelationIdList[] = $oldRelation->getId();
                            continue 2;
                        }
                    }
                    $entity->{'remove' . $singularizedKey}($oldRelation);
                    if ($key == 'sites') {
                        if (!property_exists($entity, 'deletedSites')) {
                            $entity->deletedSites = [];
                        }
                        if (method_exists($oldRelation, 'getSite')) {
                            $entity->deletedSites[] = $oldRelation->getSite()->getId();
                        } else {
                            $entity->deletedSites[] = $oldRelation->getId();
                        }
                    }
                }
                foreach ($newRelations as $newRelation) {
                    if ($newRelation->getId()) {
                        foreach ($oldRelations as $oldRelation) {
                            if (in_array($newRelation->getId(), $currentRelationIdList)) {
                                continue 2;
                            }
                        }
                    }
                    $entity->{'add' . $singularizedKey}($newRelation);
                }
                continue;
            }
            if (!method_exists($entity, 'set' . ucwords($key))) {
                continue;
            }
            if (
                isset($typeMap[$key])
                && ($typeMap[$key] == 'integer'
                    || $typeMap[$key] == 'float'
                    || $typeMap[$key] == 'smallint'
                )
                && $value === ''
            ) {
                $entity->{'set' . ucwords($key)}(null);
                continue;
            }
            if ($value === 'isNull') {
                $entity->{'set' . ucwords($key)}(null);
                continue;
            }
            $entity->{'set' . ucwords($key)}($value);
        }
        return $entity;
    }

    public function get($id, $lock = false)
    {
        $entity = $this->repository->find($id, $lock ? LockMode::OPTIMISTIC : null);
        if (!$entity) {
            $serviceName = $this->commonService->getClassName($this, true);
            throw new BadRequestException(
                BadRequestException::NO_ENTITY,
                null,
                $serviceName,
                $id
            );
        }
        return $entity;
    }

    public function __get($propertyName)
    {
        preg_match('/([a-zA-Z0-9]+)Service/i', $propertyName, $serviceMatches);
        if (count($serviceMatches) > 0) {
            return $this
                ->serviceLocator
                ->get('App\Service\\' . ucfirst($serviceMatches[1]) . 'Service');
        }

        preg_match('/([a-zA-Z0-9]+)Repo/i', $propertyName, $repositoryMatches);
        if (count($repositoryMatches) > 0) {
            return $this
                ->serviceLocator
                ->get('App\Service\\' . ucfirst($repositoryMatches[1]) . 'Service')
                ->repository;
        }

        $entityName = $this->commonService->getClassName($this, true);
        $cacheKey = 'Const.' . $entityName . '.' . $propertyName;
        $cachedConstantItem = $this->cache->getItem($cacheKey);

        if ($cachedConstantItem->isHit()) {
            return $cachedConstantItem->get();
        }

        $constant = @constant('App\Entity\\' . $entityName . '::' . $propertyName);
        if (!is_null($constant)) {
            $cachedConstantItem->set($constant);
            $this->cache->save($cachedConstantItem);
            return $constant;
        }

        $metas = $this->entityManager->getMetadataFactory()->getAllMetadata();
        foreach ($metas as $meta) {
            $classPath = $meta->getName();
            $name = strtoupper($this->commonService->toSnakeCase(str_replace('App\Entity\\', '', $classPath)));
            preg_match('/(' . $name . ')_(.+)/i', $propertyName, $matches);
            if (count($matches) > 0) {
                $prop = $matches[2];
                $constant = @constant($classPath . '::' . $prop);
                if (!is_null($constant)) {
                    $cachedConstantItem->set($constant);
                    $this->cache->save($cachedConstantItem);
                    return $constant;
                }
            }
        }

        trigger_error('Could not found property ' . $propertyName . ' in ' . $this->commonService->getClassName($this), E_USER_ERROR);
    }

    public function newEntity()
    {
        $entityName = $this->commonService->getClassName($this, true);
        $entityClass = 'App\\Entity\\' . $entityName;
        return new $entityClass();
    }

    public function getToken()
    {
        $token = $this->tokenStorage->getToken();
        if (is_null($token) || $token->getUser() === 'anon.') return null;
        return $token;
    }

    public function getUser()
    {
        $token = $this->getToken();
        if (is_null($token)) return null;
        return $token->getUser();
    }

    public function isLoggedIn()
    {
        return $this->getUser();
    }

    public function getClassProperties($className)
    {
        $reflectionClass = new \ReflectionClass($className);
        $properties = [];
        foreach ($reflectionClass->getProperties() as $property) {
            $properties[] = $property->getName();
        }
        return $properties;
    }

    public function getClassData($className)
    {
        $reader = new AnnotationReader();
        $reflectionClass = new \ReflectionClass($className);
        $data = [
            'properties'           => [],
            'transformAnnotations' => []
        ];
        foreach ($reflectionClass->getProperties() as $property) {
            $data['properties'][] = $property;
            /** @var TransformAnnotation $transformAnnotation */
            $transformAnnotation = $reader->getPropertyAnnotation($property, TransformAnnotation::class);

            if (!$transformAnnotation) {
                continue;
            }
            $data['transformAnnotations'][$property->getName()] = $transformAnnotation;
        }
        return $data;
    }

    public function getRootDir()
    {
        return $this->params->get('kernel.project_dir');
    }

    public function getPath($path)
    {
        return $this->getRootDir() . '/public' . $path;
    }

    public function saveFile($path, $data)
    {
        $this->filesystem->dumpFile($path, $data);
    }

    public function getUserCover($user)
    {
        if (!$user->getCover()) return null;
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/user/cover/%s',
                $user->getCover()
            )
        );
    }

    public function getUserPicture($user)
    {
        if (!$user->getPicture()) return null;
        return $this->urlHelper->getAbsoluteUrl(
            sprintf(
                '/uploads/user/picture/%s',
                $user->getPicture()
            )
        );
    }

    public function getCollectionProp($collection, $property = 'Id')
    {
        $propertyList = [];
        foreach ($collection as $entity) {
            $propertyList[] = $entity->{'get' . $property}();
        }
        return $propertyList;
    }

    public function isAdmin($user)
    {
        foreach ($user->getSubRoles() as $role) {
            if ($role->getName() == 'Administrator') {
                return true;
            }
        }
        return false;
    }

    public function sendMail(\Swift_Message $message)
    {
        try {
            $this->container->get('mailer')->send($message);

            return [
                'status' => 'success',
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 'failed',
                'errors' => [
                    get_class($ex) => $ex->getMessage(),
                ],
            ];
        }
    }

    public function getFolder($path)
    {
        if (!$this->filesystem->exists($path)) {
            $this->filesystem->mkdir($path, 0700);
        }
        return $path;
    }

    public function makeUploadPath($path)
    {
        $fullPath = $this->getRootDir() . '/public' . $path;
        $this->getFolder($fullPath);
        return $fullPath;
    }

    public function flatening($entity)
    {
        $flatEntity = [];
        $entityName = $this->commonService->getClassName($entity);
        $metadata = $this->getEntityManager()->getClassMetadata('App\\Entity\\' . $entityName);
        foreach ($metadata->fieldMappings as $fieldName => $fieldData) {
            $value = $entity->{'get' .  ucwords($fieldName)}();
            if ($fieldData['type'] === 'datetime') {
                $flatEntity[$fieldName] = $value->format('Y-m-d H:i:s');
            } else {
                $flatEntity[$fieldName] = $value;
            }
        }
        foreach ($metadata->associationMappings as $fieldName => $associationMapping) {
            $value = $entity->{'get' .  ucwords($fieldName)}();
            if ($associationMapping['type'] === ClassMetadataInfo::MANY_TO_ONE) {

                $flatEntity[$fieldName] = is_null($value) ? $value : $value->getId();
            }
            if (
                $associationMapping['type'] === ClassMetadataInfo::MANY_TO_MANY
                || $associationMapping['type'] === ClassMetadataInfo::ONE_TO_MANY
            ) {
                $idList = [];
                foreach ($value as $associateEntity) {
                    $idList[] = $associateEntity->getId();
                }
                $flatEntity[$fieldName] = $idList;
            }
        }
        return $flatEntity;
    }

    public function getList($request)
    {
        $requestParams = $request->query->all();
        $apiUrl = $this->params->get('app_api_url');
        $entityName = $this->commonService->toKebabCase($this->commonService->getClassName($this, true));

        $response = $this->httpClient->request(
            'GET',
            $apiUrl . '/' . $entityName . '/list',
            [
                'query' => $requestParams
            ]
        );

        return json_decode($response->getContent());
    }

    public function getListBySite($request)
    {
        $requestParams = $request->query->all();
        $apiUrl = $this->params->get('app_api_url');
        $apiToken = $this->params->get('app_api_token');
        $requestParams['key'] = $apiToken;
        $entityName = $this->commonService->toKebabCase($this->commonService->getClassName($this, true));

        $response = $this->httpClient->request(
            'GET',
            $apiUrl . '/' . $entityName . '/list-by-site',
            [
                'query' => $requestParams
            ]
        );

        return json_decode($response->getContent());
    }

    public function getOne($id)
    {
        $apiUrl = $this->params->get('app_api_url');
        $entityName = $this->commonService->toKebabCase($this->commonService->getClassName($this, true));

        $response = $this->httpClient->request(
            'GET',
            $apiUrl . '/' . $entityName . '/' . $id
        );

        return json_decode($response->getContent());
    }
}