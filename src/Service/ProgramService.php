<?php
namespace App\Service;

/**
 * Class ProgramService
 * @package App\Service
 */
class ProgramService extends BaseService
{
    /**
     * ProgramService constructor.
     */
    public function __construct(
        BaseService $baseService
    ) {
        $this->reflectFromParent($baseService);
    }
}
