<?php

namespace App\Helper;

use Intervention\Image\ImageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploadHelper
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ImageManager
     */
    private $imageManager;

    public function __construct(
        ContainerInterface $container,
        ImageManager $imageManager
    ) {
        $this->container = $container;
        $this->imageManager = $imageManager;
    }

    public function save(UploadedFile $file, $directory, $width = null, $height = null, $name = null)
    {

        if (!$name) {
            $safeFilename = uniqid();
            $name = $safeFilename . '.' . $file->guessExtension();
        }

        // save images gif
        if ($file->guessExtension() === 'gif') {
            $file->move($directory . DIRECTORY_SEPARATOR, $name);
            return $name;
        }

        // intervention
        $image = $this->imageManager->make($file);
        // Fix image orientate
        $image->orientate();

        // Resize image
        if (($width && $height)) {

            $width = $this->container->getParameter($width);
            $height = $this->container->getParameter($height);

            $image->resize($width, $height, function ($constraint) {
                // Keep ratio
                $constraint->aspectRatio();

                // Prevent upsize
                $constraint->upsize();
            });
        }

        $image->save($directory . DIRECTORY_SEPARATOR . $name);
        return $name;
    }

    public function unlink($imageName, $directory)
    {
        @unlink($directory . DIRECTORY_SEPARATOR . $imageName);
    }
}