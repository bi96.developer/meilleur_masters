<?php

namespace App\Entity;

use App\Repository\NewsRepository;
use App\Traits\EntityDateTimeAbleTrait;
use App\Traits\SeoTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class News
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    use EntityDateTimeAbleTrait;
    use SeoTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $photo;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $priority;

    /**
     * @ORM\OneToMany(targetEntity=NewsProgramSpeciality::class, mappedBy="programSpeciality", orphanRemoval=true, cascade={"persist"})
     */
    private $newsProgramSpecialities;

    public function __construct()
    {
        $this->newsProgramSpecialities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return Collection<int, NewsProgramSpeciality>
     */
    public function getNewsProgramSpecialities(): Collection
    {
        return $this->newsProgramSpecialities;
    }

    public function addNewsProgramSpeciality(NewsProgramSpeciality $newsProgramSpeciality): self
    {
        if (!$this->newsProgramSpecialities->contains($newsProgramSpeciality)) {
            $this->newsProgramSpecialities[] = $newsProgramSpeciality;
            $newsProgramSpeciality->setNews($this);
        }

        return $this;
    }

    public function removeNewsProgramSpeciality(NewsProgramSpeciality $newsProgramSpeciality): self
    {
        if ($this->newsProgramSpecialities->removeElement($newsProgramSpeciality)) {
            // set the owning side to null (unless already changed)
            if ($newsProgramSpeciality->getNews() === $this) {
                $newsProgramSpeciality->setNews(null);
            }
        }

        return $this;
    }
}