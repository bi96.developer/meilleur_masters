<?php

namespace App\Entity;

use App\Repository\MediaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MediaRepository::class)
 */
class Media
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\Column(type="integer")
     */
    private $size;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasWebp;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasWebm;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $hasThumb;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getHasWebp(): ?bool
    {
        return $this->hasWebp;
    }

    public function setHasWebp(?bool $hasWebp): self
    {
        $this->hasWebp = $hasWebp;

        return $this;
    }

    public function getHasWebm(): ?bool
    {
        return $this->hasWebm;
    }

    public function setHasWebm(?bool $hasWebm): self
    {
        $this->hasWebm = $hasWebm;

        return $this;
    }

    public function getHasThumb(): ?bool
    {
        return $this->hasThumb;
    }

    public function setHasThumb(?bool $hasThumb): self
    {
        $this->hasThumb = $hasThumb;

        return $this;
    }
}
