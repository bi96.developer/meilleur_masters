<?php

namespace App\Entity;

use App\Repository\ProgramSpecialityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProgramSpecialityRepository::class)
 */
class ProgramSpeciality
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=NewsProgramSpeciality::class, mappedBy="programSpeciality", orphanRemoval=true, cascade={"persist"})
     */
    private $newsProgramSpecialities;

    public function __construct()
    {
        $this->newsProgramSpecialities = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, NewsProgramSpeciality>
     */
    public function getNewsProgramSpecialities(): Collection
    {
        return $this->newsProgramSpecialities;
    }

    public function addNewsProgramSpeciality(NewsProgramSpeciality $newsProgramSpeciality): self
    {
        if (!$this->newsProgramSpecialities->contains($newsProgramSpeciality)) {
            $this->newsProgramSpecialities[] = $newsProgramSpeciality;
            $newsProgramSpeciality->setProgramSpeciality($this);
        }

        return $this;
    }

    public function removeNewsProgramSpeciality(NewsProgramSpeciality $newsProgramSpeciality): self
    {
        if ($this->newsProgramSpecialities->removeElement($newsProgramSpeciality)) {
            // set the owning side to null (unless already changed)
            if ($newsProgramSpeciality->getProgramSpeciality() === $this) {
                $newsProgramSpeciality->setProgramSpeciality(null);
            }
        }

        return $this;
    }
}