<?php

namespace App\Entity;

use App\Repository\CampaignRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Traits\EntityDateTimeAbleTrait;

/**
 * @ORM\Entity(repositoryClass=CampaignRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Campaign
{
    use EntityDateTimeAbleTrait;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Advertiser::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $advertiser;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $stopAtDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stopAtImpression;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stopAtClick;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $priority;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $viewerLimitTotal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $viewerLimitPerSession;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $viewerLimitReset;

    /**
     * @ORM\OneToMany(targetEntity=Advert::class, mappedBy="campaign", orphanRemoval=true, cascade={"persist", "remove"})
     */
    private $adverts;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currentImpression;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currentClick;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stopAtConversion;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $currentConversion;

    public function __construct()
    {
        $this->adverts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdvertiser(): ?Advertiser
    {
        return $this->advertiser;
    }

    public function setAdvertiser(?Advertiser $advertiser): self
    {
        $this->advertiser = $advertiser;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getStopAtDate(): ?\DateTimeInterface
    {
        return $this->stopAtDate;
    }

    public function setStopAtDate(?\DateTimeInterface $stopAtDate): self
    {
        $this->stopAtDate = $stopAtDate;

        return $this;
    }

    public function getStopAtImpression(): ?int
    {
        return $this->stopAtImpression;
    }

    public function setStopAtImpression(?int $stopAtImpression): self
    {
        $this->stopAtImpression = $stopAtImpression;

        return $this;
    }

    public function getStopAtClick(): ?int
    {
        return $this->stopAtClick;
    }

    public function setStopAtClick(?int $stopAtClick): self
    {
        $this->stopAtClick = $stopAtClick;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(?int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getViewerLimitTotal(): ?int
    {
        return $this->viewerLimitTotal;
    }

    public function setViewerLimitTotal(?int $viewerLimitTotal): self
    {
        $this->viewerLimitTotal = $viewerLimitTotal;

        return $this;
    }

    public function getViewerLimitPerSession(): ?int
    {
        return $this->viewerLimitPerSession;
    }

    public function setViewerLimitPerSession(?int $viewerLimitPerSession): self
    {
        $this->viewerLimitPerSession = $viewerLimitPerSession;

        return $this;
    }

    public function getViewerLimitReset(): ?int
    {
        return $this->viewerLimitReset;
    }

    public function setViewerLimitReset(?int $viewerLimitReset): self
    {
        $this->viewerLimitReset = $viewerLimitReset;

        return $this;
    }

    /**
     * @return Collection<int, Advert>
     */
    public function getAdverts(): Collection
    {
        return $this->adverts;
    }

    public function addAdvert(Advert $advert): self
    {
        if (!$this->adverts->contains($advert)) {
            $this->adverts[] = $advert;
            $advert->setCampaign($this);
        }

        return $this;
    }

    public function removeAdvert(Advert $advert): self
    {
        if ($this->adverts->removeElement($advert)) {
            // set the owning side to null (unless already changed)
            if ($advert->getCampaign() === $this) {
                $advert->setCampaign(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCurrentImpression(): ?int
    {
        return $this->currentImpression;
    }

    public function setCurrentImpression(?int $currentImpression): self
    {
        $this->currentImpression = $currentImpression;

        return $this;
    }

    public function getCurrentClick(): ?int
    {
        return $this->currentClick;
    }

    public function setCurrentClick(?int $currentClick): self
    {
        $this->currentClick = $currentClick;

        return $this;
    }

    public function getStopAtConversion(): ?int
    {
        return $this->stopAtConversion;
    }

    public function setStopAtConversion(?int $stopAtConversion): self
    {
        $this->stopAtConversion = $stopAtConversion;

        return $this;
    }

    public function getCurrentConversion(): ?int
    {
        return $this->currentConversion;
    }

    public function setCurrentConversion(?int $currentConversion): self
    {
        $this->currentConversion = $currentConversion;

        return $this;
    }
}
