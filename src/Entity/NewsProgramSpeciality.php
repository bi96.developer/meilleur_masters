<?php

namespace App\Entity;

use App\Repository\NewsProgramSpecialityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewsProgramSpecialityRepository::class)
 */
class NewsProgramSpeciality
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=News::class, inversedBy="newsProgramSpecialities")
     * @ORM\JoinColumn(nullable=false)
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity=ProgramSpeciality::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $programSpeciality;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNews(): ?News
    {
        return $this->news;
    }

    public function setNews(?News $news): self
    {
        $this->news = $news;

        return $this;
    }

    public function getProgramSpeciality(): ?ProgramSpeciality
    {
        return $this->programSpeciality;
    }

    public function setProgramSpeciality(?ProgramSpeciality $programSpeciality): self
    {
        $this->programSpeciality = $programSpeciality;

        return $this;
    }
}