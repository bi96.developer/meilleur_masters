<?php

namespace App\EventSubscriber;

use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use App\Service\BaseService;

class EntitySubscriber extends BaseService implements EventSubscriberInterface
{
    public function __construct(BaseService $baseService)
    {
        $this->reflectFromParent($baseService);
    }

    public function getSubscribedEvents()
    {
        return [];
    }
}
