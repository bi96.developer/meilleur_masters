<?php

namespace App\EventSubscriber;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use App\Exception\CacheHitResponse;
use App\Exception\ValidateException;
use App\Exception\ApiExceptionInterface;
use App\Exception\BadRequestException;
use App\Service\RequestService;

class KernelExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        RequestService $requestService,
        ParameterBagInterface $params
    ) {
        $this->requestService = $requestService;
        $this->params = $params;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['onKernelException', 10]
            ],
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $response = new JsonResponse();
        
        if ($exception instanceof CacheHitResponse) {
            $this->requestService->exception = $exception;
            $event->allowCustomResponseCode();
            $response->setStatusCode(($exception->getCode() == 500) ? 200 : $exception->getCode());
            $response->setData(json_decode($exception->getMessage(), 1));
            $response->headers->set('X-Cache-Hit', 'True');
            $event->setResponse($response);
        } else if ($exception instanceof ApiExceptionInterface) {
            $responseData = $exception->getMessages();
            
            if($this->params->get('kernel.environment') == 'dev') {
                $responseData['trace'] = [
                    'position' => $exception->getLine() . " of " . $exception->getFile(),
                    'stack' => $exception->getTrace()
                ];
            }

            $response->setStatusCode($responseData['code']);
            $response->setData($responseData);
            $event->setResponse($response);
            $this->requestService->exception = $exception;
            $this->requestService->exceptionCode = $responseData['code'];
            
            $this
            ->requestService
            ->saveLog(
                $event->getResponse()->getStatusCode(),
                $event->getResponse()->getContent(),
                null,
                true
            );
        } else if ($exception instanceof ForeignKeyConstraintViolationException) {
            $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            $response->setData([
              'error' => BadRequestException::ENTITY_DELETE_RELATION,
              'code' => Response::HTTP_BAD_REQUEST
            ]);
            $event->setResponse($response);
            $this->requestService->exception = $exception;
            $this->requestService->exceptionCode = Response::HTTP_BAD_REQUEST;
            
            $this
            ->requestService
            ->saveLog(
                $event->getResponse()->getStatusCode(),
                $event->getResponse()->getContent(),
                null,
                true
            );
        }
    }
}
