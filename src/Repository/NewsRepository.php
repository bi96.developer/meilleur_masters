<?php

namespace App\Repository;

use App\Entity\News;

class NewsRepository extends BaseRepository
{

    public function getListAllNews($requestData = null, $outputClass)
    {

        $nameEntity = $this->getEntityName();

        $queryBuilder = $this->createQueryBuilder($nameEntity);
        $queryBuilder->addOrderBy($nameEntity . '.id', 'DESC');

        if (!$queryBuilder || !$outputClass) {
            return $queryBuilder->getQuery()->getArrayResult();
        }

        return $this->getPagingData($queryBuilder, $requestData, $outputClass);
    }

    public function getNewsBySlug($slug, $outputClass)
    {
        $news = $this->findOneBy([
            'slug' => $slug,
            'status' => News::STATUS_ACTIVE
        ]);

        if (!$news) {
            return null;
        }

        return $this->autoMapper->map($news, $outputClass);
    }

    public function getById($id, $outputClass = null)
    {
        $news = $this->findOneBy([
            'id' => $id
        ]);

        if (!$news || !$outputClass) {
            return $news;
        }
        return $this->autoMapper->map($news, $outputClass);
    }

    public function getListLastedNews($outputClass)
    {
        $nameEntity = $this->getEntityName();

        $queryBuilder = $this->createQueryBuilder($nameEntity);
        $queryBuilder->addOrderBy($nameEntity . '.id', 'DESC');
        $queryBuilder->setMaxResults(3);
        $news = $queryBuilder->getQuery()->getResult();

        if (!$news || !$outputClass) {
            return $news;
        }

        return $this->autoMapper->mapMultiple($news, $outputClass);
    }

    public function checkExistSlug($entity, $filed, $value)
    {
        $id = $entity->getId();

        $conn = $this->getEntityManager()->getConnection();
        $stmt = $conn->executeQuery(
            'select count(id) as id from news where id != :id and slug = :slug',
            [
                'id' => $id,
                $filed => $value
            ]
        );
        $result = $stmt->fetchOne();
        return (int) $result;
    }
}